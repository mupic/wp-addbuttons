<?php

namespace TinyMceButtons;

/*
	$AddCustomButtonTinymce = new TinyMceButtons\AddButton();
	$AddCustomButtonTinymce->add('custom_buttons_tinymce', URL."js/button.js", $position = null);
	$AddCustomButtonTinymce->register_style(URL."js/button.css");
	// $AddCustomButtonTinymce->add_button('custom_buttons_tinymce2'); //other button
	add_action('init', array($AddCustomButtonTinymce, 'init'));
 */

class AddButton{
	var $scripts_buttons = array();
	var $add_buttons = array();
	var $add_styles = array();
	var $row = 1;

	function init(){
		if(!function_exists('add_action')) return false;

		add_action( 'admin_head', array($this, 'register_plugin') );
		add_action( 'current_screen',  array($this, 'add_tinymce_style') );
	}

	function add($button, $script = null, $position = null){
		$this->scripts_buttons[] = array(
			'button' => $button,
			'script' => $script,
		);
		$this->add_button($button, $position);
	}
	function add_button($button, $position = null){
		if($position){
			$this->add_buttons[] = array('position' => $position, 'button' => $button);
		}else{
			$this->add_buttons[] = $button;
		}
	}
	function register_style($style){
		$this->add_styles[] = $style;
	}

	function register_plugin(){
		if( !current_user_can('edit_posts') && !current_user_can('edit_pages') ){
			return;
		}
		// проверяем что WYSIWYG включен
		if( get_user_option('rich_editing') == 'true'){
			add_filter( 'mce_external_plugins', array($this, 'add_script') );

			if($this->row <= 1){
				add_filter( 'mce_buttons', array($this, 'register_button'), 9999 );
			}else{
				add_filter( 'mce_buttons_'.$this->row, array($this, 'register_button'), 9999 );
			}
		}
	}
	
	function add_script($plugin_array){
		if($this->scripts_buttons)
			foreach($this->scripts_buttons as $value){
				$plugin_array[$value['button']] = $value['script']; // Укажите имя ВАШЕГО файла
			}
		return $plugin_array;
	}

	function register_button($buttons){
		if($this->add_buttons)
			foreach($this->add_buttons as $button){
				if(is_array($button)){
					$position = $button['position'] > 0? $button['position'] - 1 : count($buttons) + $button['position'] + 1;
					array_splice($buttons, $position, 0, $button['button']);
				}else{
					array_push($buttons, $button);
				}
			}

		return $buttons;
	}
	
	function add_tinymce_style(){
		if($this->add_styles)
			foreach ($this->add_styles as $style) {
				add_editor_style( $style );
			}
	}
}

